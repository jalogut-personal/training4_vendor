<?php
/**
 * Vendor.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Block;

/**
 * Vendor content block
 */
class Vendor extends \Magento\Framework\View\Element\Template
{
    /**
     * @var null|\Training4\Vendor\Model\Resource\Vendor\Collection
     */
    protected $vendorCollection = null;
    /**
     * @var mixed|null
     */
    protected $currentProduct = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Training4\Vendor\Model\Resource\Vendor\Collection $collection
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Training4\Vendor\Model\Resource\Vendor\Collection $collection,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->vendorCollection = $collection;
        $this->currentProduct = $registry->registry('current_product');
        parent::__construct($context, $data);
    }

    /**
     * Gets vendor
     *
     * @return $this
     */
    public function getVendorsForCurrentProduct()
    {
        return $this->vendorCollection->productFilter($this->currentProduct);
    }

}
