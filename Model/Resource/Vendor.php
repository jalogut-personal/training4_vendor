<?php
/**
 * Vendor.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Model\Resource;

/**
 *   vendor model
 */
class Vendor extends \Magento\Framework\Model\Resource\Db\AbstractDb
{

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\Resource\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface        $storeManager
     * @param string                                            $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\Resource\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;

    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('m2t4vendor_vendor', 'vendor_id');
    }
}
