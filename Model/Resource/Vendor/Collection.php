<?php
/**
 * Collection.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Model\Resource\Vendor;

/**
 * Vendor Collection
 */
class Collection extends \Magento\Framework\Model\Resource\Db\Collection\AbstractCollection
{

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface    $entityFactory
     * @param \Psr\Log\LoggerInterface                                     $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface                    $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null          $connection
     * @param \Magento\Framework\Model\Resource\Db\AbstractDb|null    $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\Resource\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->storeManager = $storeManager;
    }

    /**
     * @var string
     */
    protected $_idFieldName = 'vendor_id';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Training4\Vendor\Model\Vendor', 'Training4\Vendor\Model\Resource\Vendor');
    }

    /**
     * Returns pairs vendor_id - name
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('vendor_id', 'name');
    }

    /**
     * Filter collection for a product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return $this
     */
    public function productFilter(\Magento\Catalog\Model\Product $product)
    {
        $this->join(
            array('v2p' => $this->getConnection()->getTableName('m2t4vendor_vendor2product')),
            'main_table.vendor_id = v2p.vendor_id'
        )->addFieldToFilter('v2p.product_id', $product->getId());

        return $this;
    }

}
