<?php
/**
 * Vendor.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Model;

/**
 * vendor model
 *
 * @method \Training4\Vendor\Model\Vendor _getResource()
 * @method \Training4\Vendor\Model\Resource\Vendor getResource()
 */
class Vendor extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Magento\Catalog\Model\Resource\Product\Collection
     */
    protected $productCollection;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Training4\Vendor\Model\Resource\Vendor');
    }

    public function __construct(
        \Magento\Catalog\Model\Resource\Product\Collection $productCollection,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\Resource\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->productCollection = $productCollection;
        parent::__construct($context, $registry, $resource, $resourceCollection);
    }

    /**
     * Get Products from Vendor
     */
    public function getProducts()
    {
        $this->productCollection->getSelect()->join(
            array('v2p' =>  $this->productCollection->getConnection()->getTableName('m2t4vendor_vendor2product')),
            'e.entity_id = v2p.product_id'
        )->where('v2p.vendor_id = ' . $this->getVendorId());

        return $this->productCollection;
    }

}
