<?php
/**
 * InstallData.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Setup;

use Training4\Vendor\Model\Vendor;
use Training4\Vendor\Model\VendorFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Vendor factory
     *
     * @var VendorFactory
     */
    private $vendorFactory;

    /**
     * Init
     *
     * @param VendorFactory $vendorFactory
     */
    public function __construct(VendorFactory $vendorFactory)
    {
        $this->vendorFactory = $vendorFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $vendors = [
            [
                'name' => 'Vendor A',
            ],
            [
                'name' => 'Vendor B',
            ],
        ];

        /**
         * Insert default vendors
         */
        foreach ($vendors as $data) {
            $this->createVendor()->setData($data)->save();
        }

        $setup->endSetup();
    }

    /**
     * Create vendor
     *
     * @return Vendor
     */
    public function createVendor()
    {
        return $this->vendorFactory->create();
    }
}
