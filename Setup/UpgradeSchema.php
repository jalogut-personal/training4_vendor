<?php
/**
 * UpgradeSchema.php
 *
 * @category  Training4
 * @package   Training4_Vendor
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training4\Vendor\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $installer = $setup;
            $installer->startSetup();

            /**
             * Create table 'm2t4vendor_vendor2product'
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable('m2t4vendor_vendor2product')
            )->addColumn(
                'vendor_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Vendor ID'
            )->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'primary' => true, 'unsigned' => true],
                'Product ID'
            )->addForeignKey(
                $installer->getFkName(
                    'm2t4vendor_vendor2product',
                    'vendor_id',
                    $installer->getTable('m2t4vendor_vendor'),
                    'vendor_id'
                ),
                'vendor_id',
                $installer->getTable('m2t4vendor_vendor'),
                'vendor_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'm2t4vendor_vendor2product',
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id'
                ),
                'product_id',
                $installer->getTable('catalog_product_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Vendor To Product Table'
            );
            $installer->getConnection()->createTable($table);

            $installer->endSetup();
        }
    }
}
